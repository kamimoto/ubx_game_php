<?php
	require_once "function.php";
	
	$_SESSION["return_url"] = $_SERVER["REQUEST_URI"];
	
	//$user_id = getUserCookie();
	
	$conn = dbConnect();
	
	if(isset($_SESSION["signup_comp"]))
	{
		unset($_SESSION["signup_comp"]);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>ユーザー登録トップ</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>

<div class="box mt18 pd10 tal notextshadow fwn">
	<div class="fcblack fwb">141222更新　○＝更新内容　■＝直近課題</div>
	<div class="fcredpink fs11 mt5">
○探索中画面のステージのナンバリング表示を修正<br>
■ダンジョン探索完了画面の作成<br>
■探索中画面のステージチップにリンクは不要<br>
■探索中のステージチップをエリア一覧・ステージ一覧の上部に表示する<br>
■探索終了時刻‐現在時刻のタイムスタンプがおかしい（secondだけの数値でもhourが表示される）<br>
■探索中ダンジョンがユーザーごとに分けられていない（SESSIONで判定しているのが原因）<br>
■エリア・ダンジョンの解放フラグ・未解放表示の設定<br>
■ページングの作成<br>
■ログインページとパスワードカラムの作成<br>
■ステータスは現状固定値のため、簡単な計算式をつくる（基本攻撃力＋ジョブ攻撃力のようなイメージ）<br>
■レベル・経験値のゲージをつくる（MAX値を設定・参照して現状値の割合を出し、ゲージのwidthにパーセント指定する）<br>
■フォームのセキュリティ（未入力にエラーを返す、スクリプトを文字列に変換するなど）
	</div>
</div>



<div class="title fs18 mt24">ユーザー登録</div>
<div class="box pd10">
	<form method="post" action="signup_confirm.php">
		<input type="hidden" name="updated" value="updated">
		<table class="form">
			<tr><th>ユーザー名</th><td><input type="text" name="user_name" value=""></td></tr>
			<tr><th>性別</th><td>
				<div class="radiowrap">
					<div class="radiobtn"><input type="radio" name="user_gender" value="male" checked>男</div>
					<div class="radiobtn"><input type="radio" name="user_gender" value="female">女</div>
				</div>
			</td></tr>
			<tr><th>ジョブ</th><td>
				<?php echo putSelect($conn, "job", "job_id", "job_name", "001", "user_job_id", false); ?>
			</td></tr>
			<tr><th>コメント</th><td><textarea name="user_comment" rows="5">よろしくお願いします！</textarea></td></tr>
			<tr><td colspan="2" class="tac"><input type="submit" value="決定"></td></tr>
		</table>
	</form>
</div>



<div class="mt18 pb18">
	<div class="mb10">▼ご登録済の方はこちら▼</div>
	<div class="btn"><a href="mypage.php">マイページへ</a></div>
</div>



</div>
</body>
</html>