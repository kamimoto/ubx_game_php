<?php
	require_once "function.php";
	
	$_SESSION["return_url"] = $_SERVER["REQUEST_URI"];
	
	$user_id = getUserCookie();
	
	$conn = dbConnect();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>エリア一覧</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="title fs18">エリア一覧</div>
<div class="box">
	<ul class="list dungeon">
		<?php putTipsDg($conn, "area", "area_id", true, false); ?>
	</ul>
</div>



<div class="mt18 pb18">
	<div class="btn"><a href="mypage.php">マイページへ</a></div>
</div>




</div>
</body>
</html>