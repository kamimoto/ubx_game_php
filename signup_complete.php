<?php
	require_once "function.php";
	if(empty($_SESSION) && !isset($_POST["updated"]))
	{
		echo "Error.";
		exit;
	}
	
	$conn = dbConnect();
	
	if(!isset($_SESSION["signup_comp"]))
	{
		$user_name    = $_SESSION["user_name"];
		$user_gender  = $_SESSION["user_gender"];
		$user_job_id  = $_SESSION["user_job_id"];
		$user_comment = $_SESSION["user_comment"];
		
		$sql = "INSERT INTO user_main(user_name, user_gender, user_job_id, user_comment, user_dt_signup, user_dt_update)
				VALUES(:user_name, :user_gender, :user_job_id, :user_comment, NOW(), NOW())";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':user_name', $user_name);
		$stmt->bindParam(':user_gender', $user_gender);
		$stmt->bindParam(':user_job_id', $user_job_id);
		$stmt->bindParam(':user_comment', $user_comment);
		$stmt->execute();
		
		$user_id = sprintf("%07d", $conn->lastInsertId());
		
		$error = $stmt->errorInfo();
		if($error[0] != "00000")
		{
			$title_txt = "登録失敗";
			$message   = "登録に失敗しました。{$error[2]}";
		}
		else
		{
			$title_txt = "登録完了";
			$message = "登録が完了しました。<br>あなたのID： ".$user_id;
		}
		
		unset($sql, $stmt, $row);
		
		$_SESSION["user_id"] = $user_id;
		$_SESSION["signup_comp"] = true;
	}
	else
	{
		$user_id = $_SESSION["user_id"];
		
		//ユーザー情報の定義
		$sql = "SELECT * FROM user_main WHERE (user_id = :user_id)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(":user_id", $user_id);
		$stmt->execute();
		while($row = $stmt->fetch())
		{
			$user_name      = $row["user_name"];
			$user_comment   = $row["user_comment"];
			$user_gender    = $row["user_gender"];
			$user_job_id    = $row["user_job_id"];
		}
		unset($sql, $stmt, $row);
		
		$title_txt = "二重登録";
		$message = "このIDは既に登録済みです。<br>あなたのID： ".$user_id;
	}
	
	$_SESSION["user_name"] = $user_name;
	$_SESSION["user_gender"] = $user_gender;
	$_SESSION["user_job_id"] = $user_job_id;
	$_SESSION["user_comment"] = $user_comment;
	
	$sql = "SELECT * FROM job WHERE (job_id = :user_job_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(':user_job_id', $user_job_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_job_name = $row['job_name'];
	}
	unset($sql, $stmt, $row);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>ユーザー登録完了</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="title fs18"><?php echo $title_txt; ?></div>
<div class="box pd10">
	<div class="nmt8"><?php echo $message; ?></div>
	<div class="main">
		<div class="user_job_img pt14 pb14"><img src="<?php echo 'img/job/'.$user_gender.'/job_'.$user_job_id.'.png'; ?>"></div>
	</div>
	<form method="post" action="mypage.php?user_id=<?php echo $user_id; ?>">
		<table class="property status player mt14">
			<tr><th>名前</th><td><?php echo $user_name; ?></td></tr>
			<tr><th>性別</th><td><?php if($user_gender == "male"){echo "男";} else{echo "女";} ?></td></tr>
			<tr><th>ジョブ</th><td><?php echo $user_job_name; ?></td></tr>
			<tr class="wordbreak"><th>コメント</th><td><?php echo $user_comment; ?></td></tr>
			<tr><td colspan="2" class="tac"><input type="submit" value="マイページへ"></td></tr>
		</table>
	</form>
</div>




</div>
</body>
</html>