<?php
	require_once "function.php";
	if(empty($_SESSION) && !isset($_POST["updated"]))
	{
		echo "Error.";
		exit;
	}
	else if(isset($_POST["updated"]))
	{
		$user_name    = chkString($_POST["user_name"], "名前");
		$user_gender  = $_POST["user_gender"];
		$user_job_id  = $_POST["user_job_id"];
		$user_comment = chkString($_POST["user_comment"], "コメント", true);
	}
	else
	{
		$user_name    = $_SESSION["user_name"];
		$user_gender  = $_SESSION["user_gender"];
		$user_job_id  = $_SESSION["user_job_id"];
		$user_comment = $_SESSION["user_comment"];
	}
	
	if(isset($_SESSION["signup_comp"]))
	{
		header("Location:signup_complete.php");
	}
	
	$_SESSION["user_name"] = $user_name;
	$_SESSION["user_gender"] = $user_gender;
	$_SESSION["user_job_id"] = $user_job_id;
	$_SESSION["user_comment"] = $user_comment;
	
	$conn = dbConnect();
	
	$sql = "SELECT * FROM job WHERE (job_id = :user_job_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(':user_job_id', $user_job_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_job_name = $row['job_name'];
	}
	unset($sql, $stmt, $row);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>ユーザー登録確認</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="title fs18">登録確認</div>
<div class="box pd10">
	<div class="nmt8">以下の内容で登録します。<br>よろしければ「登録」ボタンを押してください。</div>
	<div class="main">
		<div class="user_job_img pt14 pb14"><img src="<?php echo 'img/job/'.$user_gender.'/job_'.$user_job_id.'.png'; ?>"></div>
	</div>
	<form method="post" action="signup_complete.php">
		<input type="hidden" name="updated" value="updated">
		<table class="property status player mt14">
			<tr><th>名前</th><td><?php echo $user_name; ?></td></tr>
			<tr><th>性別</th><td><?php if($user_gender == "male"){echo "男";} else{echo "女";} ?></td></tr>
			<tr><th>ジョブ</th><td><?php echo $user_job_name; ?></td></tr>
			<tr class="wordbreak"><th>コメント</th><td><?php echo $user_comment; ?></td></tr>
			<tr><td colspan="2" class="tac"><input type="submit" value="登録"></td></tr>
		</table>
	</form>
</div>




</div>
</body>
</html>