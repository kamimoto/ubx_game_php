<?php
//var.141224a

session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');


//データベース接続----------------------------------------------------------------------------------
function dbConnect()
{
	$dbtype = "mysql";
	$sv = "localhost";
	$dbname = "kamonabe";
	$user = "root";
	$pass = "";
	
	$dsn = "$dbtype:dbname=$dbname;host=$sv";
	$conn = new PDO($dsn, $user, $pass);
	
	return $conn;
}


//ユーザーのクッキー取得----------------------------------------------------------------------------------
function getUserCookie()
{
	if(isset($_GET["user_id"]))
	{
		$user_id = $_GET["user_id"];
	}
	else
	{
		if(!isset($_COOKIE["user_id"]))
		{
			echo "Error.";
			exit;
		}
		else
		{
			$user_id = $_COOKIE["user_id"];
		}
	}
	
	$limit = time()+3600*24*7;
	setcookie('user_id', $user_id, $limit);
	
	return $user_id;
}


//各テーブルのID（主キー）取得----------------------------------------------------------------------------------
function getSomeId($some_id)
{
	if(!isset($some_id))
	{
		errorView();
	}
	return $some_id;
}




//エラーページの表示----------------------------------------------------------------------------------
function errorView($error_txt = "エラーが発生しました。")
{
		$_SESSION["error_txt"] = $error_txt;
		header("Location:error.php");
		exit;
}


//入力値の検証・加工----------------------------------------------------------------------------------
function chkString($temp = "", $field, $accept_empty = false)
{
	if(empty($temp) AND !$accept_empty)
	{
		errorView("{$field}を入力してください。");
	}
	$temp = htmlspecialchars($temp, ENT_QUOTES, "UTF-8");
	return $temp;
}


//時間表示変換----------------------------------------------------------------------------------
function s2h($time_seconds)
{
	$nohour    = false;
	$nominutes = false;
	
	$time_hours = floor($time_seconds / 3600);
	if($time_hours == 0)
	{
		$nohour = true;
	}
	$time_minutes = floor(($time_seconds / 60) % 60);
	
	if($nohour == true)
	{
		$hms = sprintf("%02d分", $time_minutes);
	}
	else
	{
		$hms = sprintf("%02d時間%02d分", $time_hours, $time_minutes);
	}
	
	return [$hms, $time_hours, $time_minutes];
}




//セレクトボックス生成----------------------------------------------------------------------------------
function putSelect($conn, $table, $value, $text, $selected_value = "", $select_name, $this_form)
{
	$sql = "SELECT * FROM ".$table." ORDER BY ".$value;
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	echo "<select name=\"".$select_name."\"";
	if($this_form){echo " onchange=\"submit(this.form)\"";}
	echo ">\n";
	while($rows = $stmt->fetch())
	{
		echo "<option ";
		echo " value=\"".$rows[$value]."\"";
		if($selected_value == $rows[$value]){echo " selected";}
		echo ">".$rows[$text]."</option>\n";
	}
	echo "</select>\n";
	unset($sql, $stmt, $rows, $this_form);
}





//ダンジョンチップ生成----------------------------------------------------------------------------------
function putTipsDg($conn, $table, $value, $area, $area_id, $searching_dungeon = false)
{
	$count = 1;
	
	if($area == true)
	{
		$sql = "SELECT * FROM ".$table." ORDER BY ".$value;
	}
	else if(empty($searching_dungeon))
	{
		$sql = "SELECT * FROM ".$table." WHERE (dungeon_area_id = :area_id)";
	}
	else
	{
		$sql = "SELECT * FROM ".$table." WHERE (dungeon_id = :searching_dungeon)";
	}
	$stmt = $conn->prepare($sql);
	if(empty($area))
	{
		if($searching_dungeon == false)
		{
			$stmt->bindParam(":area_id", $area_id);
		}
		else
		{
			$stmt->bindParam(":searching_dungeon", $searching_dungeon);
		}
	}
	$stmt->execute();
	while($rows = $stmt->fetch())
	{
		if($area == true)
		{
			$target = "dungeon_list.php";
			$text   = "エリア";
			$id     = $rows["area_id"];
			$name   = $rows["area_name"];
			$path   = "img/dungeon/area_".$id.".jpg";
		}
		else
		{
			$target  = "dungeon_start.php";
			$text    = "ステージ";
			$area_id = $rows["dungeon_area_id"];
			$id      = $rows["dungeon_id"];
			$name    = $rows["dungeon_name"];
			$dfc     = $rows["dungeon_difficulty"];
			$span    = $rows["dungeon_time_span"];
			$path    = "img/dungeon/area_".$area_id.".jpg";
			
			$time = s2h($span);
		}
		echo "<li>\n";
		if(!$searching_dungeon) echo "<a href=\"".$target."?".$value."=".$id."\">\n";
		echo "<div class=\"label\"><span class=\"area_num\">".$text;
		echo !$searching_dungeon ? $count : "00";
		echo "</span><span class=\"area_name\">".$name."</span></div>\n";
		echo "<div class=\"img\"><img src=\"".$path."\"></div>\n";
		if($area == true || $searching_dungeon)
		{
			if(!$searching_dungeon) echo "</a>\n";
			echo "</li>\n";
		}
		else
		{
			echo "</a>\n";
			echo "<div class=\"txt\">\n<span class=\"dungeon_difficulty\">難易度 ";
			for($i=1; $i<=$dfc; $i++)
			{
				echo "<span class=\"icon star\">★</span>";
			}
			echo "</span><span class=\"dungeon_time_span\">探索時間 ".$time[0]."</span>\n</div>\n";
			echo "</li>\n";
		}
		
		$count++;
	}
	unset($sql, $stmt, $rows);
}


//ダンジョン探索設定----------------------------------------------------------------------------------
function setUserDg($conn, $user_id, $dungeon_id)
{
	$sql = "SELECT * FROM dungeon WHERE (dungeon_id = :dungeon_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":dungeon_id", $dungeon_id);
	$stmt->execute();
	while($rows = $stmt->fetch())
	{
		$span = $rows["dungeon_time_span"];
		
		$time = s2h($span);
	}
	unset($sql, $stmt, $rows);
	
	
	
	//現在時刻の情報を持った配列を作成
	$now = getdate();
	$month   = $now['mon'];
	$today   = $now['mday'];
	$year    = $now['year'];
	$hours   = $now['hours'];
	$minutes = $now['minutes'];
	$seconds = $now['seconds'];
	
	//現在時刻のタイムスタンプを取得→日付に変換
	$now_time_st = mktime($hours, $minutes, $seconds, $month, $today, $year);
	$now_time    = date("Y-m-d H:i:s", $now_time_st);
	
	if(empty($_SESSION["dungeon_searching"]))
	{
		//終了時刻のタイムスタンプを取得→日付に変換
		$start_time_st = $now_time_st;
		$start_time    = $now_time;
		$end_time_st   = mktime($hours+$time[1], $minutes+$time[2], $seconds, $month, $today, $year);
		$end_time      = date("Y-m-d H:i:s", $end_time_st);
		
		$dungeon_end = $end_time;
		
		$sql = "UPDATE user_main SET
					user_dungeon_id  = :dungeon_id,
					user_dungeon_end = :dungeon_end
				WHERE (user_id = :user_id)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(":user_id", $user_id);
		$stmt->bindParam(":dungeon_id", $dungeon_id);
		$stmt->bindParam(":dungeon_end", $dungeon_end, PDO::PARAM_STR);
		$stmt->execute();
		unset($sql, $stmt, $rows);
		
		$sql = "SELECT user_dungeon_id, user_dungeon_end FROM user_main WHERE (user_id = :user_id)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(":user_id", $user_id);
		$stmt->execute();
		while($rows = $stmt->fetch())
		{
			$user_dungeon_id  = $rows["user_dungeon_id"];
			$user_dungeon_end = $rows["user_dungeon_end"];
		}
		unset($sql, $stmt, $rows);
		
		echo $user_id."<br>";
		echo $user_dungeon_id."<br>";
		echo $user_dungeon_end."<br>";
		
		$_SESSION["dungeon_searching"] = true;
		$_SESSION["user_dungeon_id"] = $user_dungeon_id;
	}
	else
	{
		$user_dungeon_id = $dungeon_id;
		$end_time_st = getUserDg($conn, $dungeon_id);
		$diff = ($end_time_st-$now_time_st)-(9*60*60);
		$span_time_st = date("H:i:s", $diff);
		echo "あと".$span_time_st."<br>";
		finishUserDg($now_time_st, $end_time_st);
	}
	
	return $user_dungeon_id;
}


//ダンジョン探索終了判定
function finishUserDg($now_time, $finish_time)
{
	echo $finish_time."<br>";
	echo $now_time."<br>";
	
	if($finish_time <= $now_time)
	{
		unset($_SESSION["dungeon_searching"]);
//		echo "探索完了！";
		header("Location:dungeon_finished.php");
		$sql = "UPDATE user_main SET
					user_dungeon_id  = NULL,
					user_dungeon_end = NULL
				WHERE (user_id = :user_id)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(":user_id", $user_id);
		$stmt->execute();
		unset($sql, $stmt, $rows);
		exit;
	}
}


//探索中のダンジョン
function getUserDg($conn, $user_dungeon_id)
{
	$sql = "SELECT * FROM user_main WHERE (user_dungeon_id = :user_dungeon_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_dungeon_id", $user_dungeon_id);
	$stmt->execute();
	while($rows = $stmt->fetch())
	{
		$user_dungeon_id  = $rows["user_dungeon_id"];
		$user_dungeon_end = $rows["user_dungeon_end"];
	}
	unset($sql, $stmt, $rows);
	return strtotime($user_dungeon_end);
}






?>