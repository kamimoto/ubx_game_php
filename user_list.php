<?php
	require_once "function.php";
	
	$user_id = getUserCookie();
	$_SESSION["user_id"] = $user_id;
	
	$conn = dbConnect();
	
	
	
	//ユーザーステータス更新用
	if(isset($_POST["updated"]))
	{
		$_SESSION["user_name"]     = $_POST["user_name"];
		$_SESSION["user_gender"]   = $_POST["user_gender"];
		$_SESSION["user_job_id"]   = $_POST["user_job_id"];
		$_SESSION["user_title_id"] = $_POST["user_title_id"];
		$_SESSION["user_comment"]  = $_POST["user_comment"];
		
		$user_id      = $_SESSION["user_id"];
		$user_name    = $_SESSION["user_name"];
		$user_gender  = $_SESSION["user_gender"];
		$user_job_id  = $_SESSION["user_job_id"];
		$user_title_id  = $_SESSION["user_title_id"];
		$user_comment = $_SESSION["user_comment"];
		
		$sql = "UPDATE user_main SET
					user_name      = :user_name,
					user_dt_update = NOW(),
					user_gender    = :user_gender,
					user_job_id    = :user_job_id,
					user_title_id  = :user_title_id,
					user_comment   = :user_comment
				WHERE user_id = :user_id";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(":user_id", $user_id);
		$stmt->bindParam(":user_gender", $user_gender);
		$stmt->bindParam(":user_name", $user_name);
		$stmt->bindParam(":user_job_id", $user_job_id);
		$stmt->bindParam(":user_title_id", $user_title_id);
		$stmt->bindParam(":user_comment", $user_comment);
		$stmt->execute();
		unset($sql, $stmt, $row);
	}
	
	
	
	//ユーザー情報の定義
	$sql = "SELECT * FROM user_main WHERE (user_id = :user_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_id", $user_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_dt_signup = $row["user_dt_signup"];
		$user_dt_update = $row["user_dt_update"];
		$user_name      = $row["user_name"];
		$user_comment   = $row["user_comment"];
		$user_title_id  = $row["user_title_id"];
		$user_par_lv    = $row["user_par_lv"];
		$user_par_exp   = $row["user_par_exp"];
		$user_par_ap    = $row["user_par_ap"];
		$user_gender    = $row["user_gender"];
		$user_job_id    = $row["user_job_id"];
		$user_par_hp    = $row["user_par_hp"];
		$user_par_atk   = $row["user_par_atk"];
		$user_par_def   = $row["user_par_def"];
		$user_par_matk  = $row["user_par_matk"];
		$user_par_mdef  = $row["user_par_mdef"];
	}
	unset($sql, $stmt, $row);
	
	$sql = "SELECT * FROM title WHERE (title_id = :user_title_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_title_id", $user_title_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_title_name = $row["title_name"];
	}
	unset($sql, $stmt, $row);
	
	$sql = "SELECT * FROM job WHERE (job_id = :user_job_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_job_id", $user_job_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_job_name = $row["job_name"];
	}
	unset($sql, $stmt, $row);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>ユーザー一覧</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="pd10 mt10 mb10">
	<form method="get" action="user_list.php">
		ユーザーID　<?php echo putSelect($conn, "user_main", "user_id", "user_id", $user_id, "user_id", true); ?>
	</form>
</div>



<div class="title fs18">プレイヤーステータス</div>
<div class="box pd10">
	<div class="main">
		<div class="namebox">
			<div class="user_title"><?php echo $user_title_name; ?></div>
			<div class="user_name"><?php echo $user_name; ?></div>
		</div>
		
		<div class="user_job_img"><img src="<?php echo "img/job/".$user_gender."/job_".$user_job_id.".png"; ?>"></div>
		<div class="guagebox">
			<div class="user_par_lv">Lv <?php echo $user_par_lv; ?>　EXP <?php echo $user_par_exp; ?></div>
			<div class="user_par_ap">AP <?php echo $user_par_ap; ?></div>
		</div>
	</div>
	<table class="property status player mt14">
		<tr><th>ID</th><td><?php echo $user_id; ?></td></tr>
		<tr><th>登録日時</th><td><?php echo $user_dt_signup; ?></td></tr>
		<tr><th>更新日時</th><td><?php echo $user_dt_update; ?></td></tr>
		<tr class="wordbreak"><th>コメント</th><td><?php echo $user_comment; ?></td></tr>
		<tr><th>ジョブ</th><td><?php echo $user_job_name; ?></td></tr>
		<tr><th>HP</th><td><?php echo $user_par_hp; ?></td></tr>
		<tr><th>攻撃力</th><td><?php echo $user_par_atk; ?></td></tr>
		<tr><th>防御力</th><td><?php echo $user_par_def; ?></td></tr>
		<tr class="user_par_atk"><th>魔法攻撃力</th><td><?php echo $user_par_matk; ?></td></tr>
		<tr class="user_par_atk"><th>魔法防御力</th><td><?php echo $user_par_mdef; ?></td></tr>
	</table>
</div>



<div class="title fs18 mt24">ステータス変更</div>
<div class="box pd10">
	<form method="post" action="user_list.php?user_id=<?php echo $user_id; ?>">
		<input type="hidden" name="updated" value="updated">
		<table class="form">
			<tr><th>ユーザー名</th><td><input type="text" name="user_name" value="<?php echo $user_name; ?>"></td></tr>
			<tr><th>性別</th><td>
				<div class="radiowrap">
					<div class="radiobtn"><input type="radio" name="user_gender" value="male"<?php if($user_gender == "male"){echo " checked";}; ?>>男</div>
					<div class="radiobtn"><input type="radio" name="user_gender" value="female"<?php if($user_gender == "female"){echo " checked";}; ?>>女</div>
				</div>
			</td></tr>
			<tr><th>ジョブ</th><td>
				<?php echo putSelect($conn, "job", "job_id", "job_name", $user_job_id, "user_job_id", false); ?>
			</td></tr>
			<tr><th>称号</th><td>
				<?php echo putSelect($conn, "title", "title_id", "title_name", $user_title_id, "user_title_id", false); ?>
			</td></tr>
			<tr><th>コメント</th><td><textarea name="user_comment" rows="5"><?php echo $user_comment; ?></textarea></td></tr>
			<tr><td colspan="2" class="tac"><input type="submit" value="変更する"></td></tr>
		</table>
	</form>
</div>



<div class="mt18 pb18">
	<div class="btn"><a href="mypage.php">このユーザーのマイページへ</a></div>
	
	<div class="btn mt14"><a href="index.php">新規ユーザー登録</a></div>
</div>




</div>
</body>
</html>