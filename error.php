<?php
	require_once "function.php";
	
	$error_txt  = $_SESSION["error_txt"];
	$return_url = $_SESSION["return_url"];
	
	$conn = dbConnect();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>エラー</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="title fs18">エラー</div>
<div class="box pd10">
	<div class="pt18 pb18 fs18 fcred"><?php echo $error_txt; ?></div>
</div>

<div class="mt18 pb18">
	<div class="btn"><a href="<?php echo $return_url ?>">戻る</a></div>
	
	<div class="btn mt14"><a href="mypage.php">マイページ</a></div>
</div>




</div>
</body>
</html>