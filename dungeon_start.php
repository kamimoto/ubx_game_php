<?php
	require_once "function.php";
	
	$user_id = getUserCookie();
	
	if(!isset($_SESSION["dungeon_searching"]))
	{
		$dungeon_id = getSomeId($_GET["dungeon_id"]);
	}
	else
	{
		$dungeon_id = $_SESSION["user_dungeon_id"];
	}
	
	$conn = dbConnect();
	
	$user_dungeon_id = setUserDg($conn, $user_id, $dungeon_id);
	
	$sql = "SELECT * FROM dungeon WHERE (dungeon_id = :user_dungeon_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_dungeon_id", $user_dungeon_id);
	$stmt->execute();
	while($rows = $stmt->fetch())
	{
		$dungeon_id      = $rows["dungeon_id"];
		$dungeon_area_id = $rows["dungeon_area_id"];
	}
	unset($sql, $stmt, $rows);
	
	$area_id = $dungeon_area_id;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>探索中</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="title fs18">探索中のステージ</div>
<div class="box">
	<ul class="list dungeon">
		<?php putTipsDg($conn, "dungeon", "dungeon_id", false, $area_id, $user_dungeon_id); ?>
	</ul>
</div>



<div class="mt18 pb18">
	<div class="btn"><a href="dungeon_list.php<?php echo "?area_id=".$area_id ?>">ステージ一覧へ</a></div>
	
	<div class="btn mt18"><a href="dungeon.php">エリア一覧へ</a></div>
	
	<div class="btn mt18"><a href="mypage.php">マイページへ</a></div>
</div>




</div>
</body>
</html>