<?php
	require_once "function.php";
	
	$user_id = getUserCookie();
	$_SESSION["user_id"] = $user_id;
	
	$conn = dbConnect();
	
	
	//ユーザー情報の定義
	$sql = "SELECT * FROM user_main WHERE (user_id = :user_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_id", $user_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_dt_signup = $row["user_dt_signup"];
		$user_dt_update = $row["user_dt_update"];
		$user_name      = $row["user_name"];
		$user_comment   = $row["user_comment"];
		$user_title_id  = $row["user_title_id"];
		$user_par_lv    = $row["user_par_lv"];
		$user_par_exp   = $row["user_par_exp"];
		$user_par_ap    = $row["user_par_ap"];
		$user_gender    = $row["user_gender"];
		$user_job_id    = $row["user_job_id"];
		$user_par_hp    = $row["user_par_hp"];
		$user_par_atk   = $row["user_par_atk"];
		$user_par_def   = $row["user_par_def"];
		$user_par_matk  = $row["user_par_matk"];
		$user_par_mdef  = $row["user_par_mdef"];
	}
	unset($sql, $stmt, $row);
	
	$sql = "SELECT * FROM title WHERE (title_id = :user_title_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_title_id", $user_title_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_title_name = $row["title_name"];
	}
	unset($sql, $stmt, $row);
	
	$sql = "SELECT * FROM job WHERE (job_id = :user_job_id)";
	$stmt = $conn->prepare($sql);
	$stmt->bindParam(":user_job_id", $user_job_id);
	$stmt->execute();
	while($row = $stmt->fetch())
	{
		$user_job_name = $row["job_name"];
	}
	unset($sql, $stmt, $row);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no">

<title>マイページ</title>

<link rel="stylesheet" type="text/css" href="css/reset.css">
<link rel="stylesheet" type="text/css" href="css/common.css" class="cssfx">
<script src="js/jquery-2.0.3.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.transit.js"></script>
</head>

<body>
<div id="container" class="pt18 pb18">

<div class="mb10">
	<h1>DBテスト</h1>
</div>


<div class="title fs18">プレイヤーステータス</div>
<div class="box pd10">
	<div class="main">
		<div class="namebox">
			<div class="user_title"><?php echo $user_title_name; ?></div>
			<div class="user_name"><?php echo $user_name; ?></div>
		</div>
		
		<div class="user_job_img"><img src="<?php echo "img/job/".$user_gender."/job_".$user_job_id.".png"; ?>"></div>
		<div class="guagebox">
			<div class="user_par_lv">Lv <?php echo $user_par_lv; ?>　EXP <?php echo $user_par_exp; ?></div>
			<div class="user_par_ap">AP <?php echo $user_par_ap; ?></div>
		</div>
	</div>
	
	<div class="btn medium mt14"><a href="dungeon.php">ダンジョン探索へ</a></div>
	
	<table class="property status player mt14">
		<tr><th>ID</th><td><?php echo $user_id; ?></td></tr>
		<tr><th>登録日時</th><td><?php echo $user_dt_signup; ?></td></tr>
		<tr><th>更新日時</th><td><?php echo $user_dt_update; ?></td></tr>
		<tr class="wordbreak"><th>コメント</th><td><?php echo $user_comment; ?></td></tr>
		<tr><th>ジョブ</th><td><?php echo $user_job_name; ?></td></tr>
		<tr><th>HP</th><td><?php echo $user_par_hp; ?></td></tr>
		<tr><th>攻撃力</th><td><?php echo $user_par_atk; ?></td></tr>
		<tr><th>防御力</th><td><?php echo $user_par_def; ?></td></tr>
		<tr class="user_par_atk"><th>魔法攻撃力</th><td><?php echo $user_par_matk; ?></td></tr>
		<tr class="user_par_atk"><th>魔法防御力</th><td><?php echo $user_par_mdef; ?></td></tr>
	</table>
</div>



<div class="mt18 pb18">
	<div class="btn"><a href="user_list.php">ユーザー一覧</a></div>
	
	<div class="btn mt14"><a href="index.php">新規ユーザー登録（トップページ）</a></div>
</div>




</div>
</body>
</html>